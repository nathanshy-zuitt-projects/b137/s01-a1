package com.zuitt.activity;

public class ReportCard {

    public static void main (String[] args) {
        // Declare variables
        String firstName;
        String lastName;
        double englishGrade;
        double mathGrade;
        double scienceGrade;

        // Initialize here for separation
        firstName = "Nathan";
        lastName = "Shy";
        englishGrade = 87.88;
        mathGrade = 88.87;
        scienceGrade = 75.64;

        // Get average grade
        double averageGrade = (mathGrade + englishGrade + scienceGrade) / 3;

        // Print details

        System.out.println("Student Name:  " + firstName + " " + lastName);
        System.out.println("Average Grade: " + averageGrade);
    }
}
